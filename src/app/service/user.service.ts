import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../model/user';
import  {HttpClient} from '@angular/common/http';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private router:Router,private http:HttpClient) { }

  isLoggedIn(){
    return !!localStorage.getItem('email');
  }

  url:string='http://localhost:8080/';

  checkLogin(user:LoginUser):Observable<Message>{
    // if(user.email==="shalini" && user.password==="shalini")
    //   return true;
    return this.http.post<Message>(this.url + 'authenticate', user);
  
  }
  
  logout()
  {
    localStorage.removeItem('email');
    this.router.navigate(['']);
  }
}
