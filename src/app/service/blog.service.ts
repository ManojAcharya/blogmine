import { Injectable } from '@angular/core';
import { Blog } from '../model/Blog';
import { blogs } from '../model/bloglist';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  bloglist: Blog[];
  constructor() {
    this.bloglist = blogs;
  }

  getBlogById(id: number): any {
    return this.bloglist.find(blog => blog.id === id);
  }
  addBlog(blog: Blog) {
    let id = this.bloglist.length + 1;
    blog.id = id;
    this.bloglist.push(blog);
    console.log(this.bloglist);
  }
  getBlogs() {
    console.log('service')
    console.log(this.bloglist)
    return this.bloglist;
  }

  deleteBlog(blog: Blog) {
    console.log('in service deleting...')
    console.log(blog)
    for (var j = 0; j < this.bloglist.length; j++) {
      if (this.bloglist[j] === blog) {
        this.bloglist.splice(j, 1);
        console.log('deleted')
        console.log(this.bloglist)
      }
    }
  }
  editBlog(blog: Blog) {
    for (var j = 0; j < this.bloglist.length; j++) {
      if (this.bloglist[j].id === blog.id) {
        this.bloglist[j].title = blog.title;
        this.bloglist[j].description = blog.description;
        this.bloglist[j].category = blog.category;
      }
    }
  }
}
