import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Blog } from '../model/Blog';
import { categories } from '../model/bloglist';
import { BlogService } from '../service/blog.service';

@Component({
  selector: 'app-blogedit',
  templateUrl: './blogedit.component.html',
  styleUrls: ['./blogedit.component.css']
})
export class BlogeditComponent implements OnInit {

  categories;
  blog: Blog;
  hasError: boolean = false;

  constructor(private service: BlogService, private route: ActivatedRoute,private router:Router) {
    this.blog = new Blog('', '', '', '');
    this.categories = categories;
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
      let id1:string=<string>params.get('id');
      this.blog=this.service.getBlogById(parseInt(id1));
    });
  }
  
  validate(category: string) {
    if (category === 'default') {
      this.hasError = true;
    }
    else {
      this.hasError = false;
    }

  }

  onSubmit() {
    console.log(this.blog);
    this.service.editBlog(this.blog);
    this.router.navigate(['list']);
  }

}