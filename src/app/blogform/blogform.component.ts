import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Blog } from '../model/Blog';
import { categories } from '../model/bloglist';
import { BlogService } from '../service/blog.service';

@Component({
  selector: 'app-blogform',
  templateUrl: './blogform.component.html',
  styleUrls: ['./blogform.component.css']
})
export class BlogformComponent implements OnInit {

  categories;
  blog:Blog
  hasError:boolean = true;
  month = ['Jan','Feb','March','April','May','June','July','Aug','Sep','Oct','Nov','Dec'];

  constructor(private bs:BlogService,private router:Router) { 
    this.blog = new Blog('','','','default');
    this.categories = categories;
  }

  ngOnInit(): void {
  }

  validate(category:string)
  {
    if(category === 'default')
    {
      this.hasError = true;
    }
    else {
      this.hasError = false;
    }

  }

  showMsg:boolean=false;

  onSubmit()
  {
    let today = new Date();
    let date = today.getDate() + ' '+this.month[today.getMonth()] +' '+today.getFullYear();
    this.blog.date = date;
    console.log(this.blog);
    
      this.bs.addBlog(this.blog);
      this.showMsg=true;
      this.router.navigate(['list']);
   
  }

}
