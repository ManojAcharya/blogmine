import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Blog } from '../model/Blog';
import { BlogService } from '../service/blog.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {

  blogs: Blog[];
  selid: number;
  constructor(private bs: BlogService, private router: Router, private route: ActivatedRoute) {
    this.blogs = [];
    this.selid = 0;
  }

  ngOnInit(): void {
    this.blogs = this.bs.getBlogs();
    console.log(this.blogs);
    this.route.queryParams.subscribe((params) => {
      this.selid = parseInt(params['id']);
    });
  }

  delete(blog: Blog) {
    console.log('deleting....');
    this.bs.deleteBlog(blog);
  }

  edit(blog: Blog) {
    this.router.navigate(['/edit', blog.id]);
  }
  onselect(blog: Blog) {
    this.router.navigate([blog.id], { relativeTo: this.route, queryParams: {} });
  }
  isselected(blog: Blog) {
    console.log('is selected ****')
    console.log(blog)
    console.log(this.selid, blog.id)
    return this.selid === blog.id;
  }
}
