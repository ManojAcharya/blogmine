import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-blogdetail',
  templateUrl: './blogdetail.component.html',
  styleUrls: ['./blogdetail.component.css']
})
export class BlogdetailComponent implements OnInit {
  id:number;
  constructor(private router:Router, private route:ActivatedRoute) { 
    this.id=0;
  }
 
  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      console.log(params.get('id'))
      this.id=parseInt(<string>params.get('id'));
    })
  }

  back()
{
	//this.router.navigate(['/list',{id:this.id}])
  this.router.navigate(['../'],{relativeTo:this.route,queryParams:{id:this.id}});

}

}
