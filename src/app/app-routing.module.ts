import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogdetailComponent } from './blogdetail/blogdetail.component';
import { BlogeditComponent } from './blogedit/blogedit.component';
import { BlogformComponent } from './blogform/blogform.component';
import { BlogsComponent } from './blogs/blogs.component';
import { FooterComponent } from './footer/footer.component';
import { AuthguardsService } from './guards/authguards.service';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path:'',redirectTo:'list',pathMatch:'full'},
  {path:'list',component:BlogsComponent},
  {path:'list/:id',component:BlogdetailComponent},
  {path:'create',component:BlogformComponent, canActivate:[AuthguardsService]},
  {path:'edit/:id',component:BlogeditComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'**',component:PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
