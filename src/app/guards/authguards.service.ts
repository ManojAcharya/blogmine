import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../service/user.service';
@Injectable({
  providedIn: 'root'
})
export class AuthguardsService implements CanActivate{

  constructor(private us:UserService, private router:Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    console.log('auth guard');
    if(this.us.isLoggedIn())
      return true;
      else
    {
      this.router.navigate(['/login']);
      return false;
    }
  }

  
}
